package ru.alfabank.urpip.grampus.debug;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import ru.alfabank.orrpp.common.util.Bytes;

import codec.asn1.ASN1Exception;
import codec.asn1.ASN1Type;
import codec.asn1.DERDecoder;
import codec.asn1.DEREncoder;

public class ASN1Util {
    
    private static final Logger LOGGER = Logger.getLogger(ASN1Util.class.getName());
    
    /**
     * Не инстанциируемый класс
     */
    private ASN1Util() {
    }
    
    public static byte[] encode(ASN1Type asn1AwareObject) throws ASN1EncodeException {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            try (DEREncoder encoder = new DEREncoder(baos)) {
                asn1AwareObject.encode(encoder);
            }
            byte[] encodedBytes = baos.toByteArray();
            if (LOGGER.isLoggable(Level.FINEST)) {
                LOGGER.log(Level.FINEST, "ASN.1/DER encoding object of class={0}, bytes: {1}", new Object[] {asn1AwareObject.getClass().getName(), Bytes.renderBytes(encodedBytes)});
            }
            return encodedBytes;
        } catch (ASN1Exception e) {
            throw new ASN1EncodeException("Error while encoding object of class " + asn1AwareObject.getClass().getName(), asn1AwareObject.getClass(), e);
        } catch (IOException e) {
            throw new WrapperForImpossibleException(e);
        }
    }
    
    public static void decode(byte[] encodedBytes, ASN1Type asn1AwareObjectPrototype) throws ASN1DecodeException {
        if (LOGGER.isLoggable(Level.FINEST)) {
            LOGGER.log(Level.FINEST, "ASN.1/DER decoding object of class={0}, bytes: {1}", new Object[] {asn1AwareObjectPrototype.getClass().getName(), Bytes.renderBytes(encodedBytes)});
        }
        try (ByteArrayInputStream bais = new ByteArrayInputStream(encodedBytes); DERDecoder decoder = new DERDecoder(bais)) {
            asn1AwareObjectPrototype.decode(decoder);
        } catch (ASN1Exception | EOFException e) {
            throw new ASN1DecodeException("Error while decoding object of class " + asn1AwareObjectPrototype.getClass().getName(), asn1AwareObjectPrototype.getClass(), e);
        } catch (IOException e) {
            throw new WrapperForImpossibleException(e);
        }
    }
    
}
