package ru.alfabank.urpip.grampus.debug;

import codec.asn1.ASN1Type;

public class ASN1DecodeException extends Exception {
    
    private static final long serialVersionUID = 1L;
    private Class<? extends ASN1Type> asn1AwareObjectPrototypeClass;
    
    public ASN1DecodeException(String message, Class<? extends ASN1Type> asn1AwareObjectPrototypeClass) {
        super(message);
        this.asn1AwareObjectPrototypeClass = asn1AwareObjectPrototypeClass;
    }
    
    public ASN1DecodeException(String message, Class<? extends ASN1Type> asn1AwareObjectPrototypeClass, Throwable cause) {
        super(message, cause);
        this.asn1AwareObjectPrototypeClass = asn1AwareObjectPrototypeClass;
    }
    
    public Class<? extends ASN1Type> getAsn1AwareObjectPrototypeClass() {
        return asn1AwareObjectPrototypeClass;
    }
    
}
