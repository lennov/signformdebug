package ru.alfabank.urpip.grampus.debug;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import ru.alfabank.orrpp.common.util.Bytes;
import ru.alfabank.urpip.debug.common.AttachmentInfo;
import ru.alfabank.urpip.debug.common.CurrencyDocumentAttachment;
import ru.alfabank.urpip.debug.common.HashUtils;
import ru.alfabank.urpip.debug.common.StringUtils;
import ru.alfabank.urpip.debug.common.TBSInfo;

import codec.pkcs7.ContentInfo;
import codec.pkcs7.SignedData;
import com.google.gwt.thirdparty.guava.common.io.Files;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.JavaScript;
import com.vaadin.ui.JavaScriptFunction;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;
import net.sourceforge.migbase64.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SignForm2 extends Window {

	private static final String SECOND_ATTACHMENT_CONTENT = "SECOND_ATTACHMENT_CONTENT";

	private static final String FIRST_ATTACHMENT_CONTENT = "FIRST_ATTACHMENT_CONTENT";

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(SignForm2.class.getName());

	private VerticalLayout mainLayout;
	private boolean isPluginInstalled;
	private boolean debug = false;
	private VerticalLayout attachmentsLayout;
	private Label requestHashLabel;

	private TextArea text;

	public SignForm2() {
		try{
			Files.write(FIRST_ATTACHMENT_CONTENT.getBytes(), new File("W:\\tmp\\first_content.txt"));
			Files.write(SECOND_ATTACHMENT_CONTENT.getBytes(), new File("W:\\tmp\\second_content.txt"));
		}catch(Exception e){
			e.printStackTrace();
		}
		
		initForm();
		initSkeleton();
		initScripts();
		JavaScript.getCurrent().execute("initNewSignForm()");

		TBSInfo tbsInfo = new TBSInfo();
		tbsInfo.setIdState(123456);
		tbsInfo.setTbsText(
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");

		List<CurrencyDocumentAttachment> attachments = new ArrayList<CurrencyDocumentAttachment>();
		CurrencyDocumentAttachment attachment1 = new CurrencyDocumentAttachment();
		attachment1.setId(1111111);
		attachment1.setFilename("attachment1.txt");
		try {
			attachment1.setGOSTDigest(HashUtils.computeGOSTHash(FIRST_ATTACHMENT_CONTENT.getBytes()));
			attachment1.setSHA1Digest(HashUtils.computeHash(FIRST_ATTACHMENT_CONTENT.getBytes(), "SHA1"));
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		} catch (NoSuchProviderException e) {
			throw new RuntimeException(e);
		}
		attachments.add(attachment1);
		CurrencyDocumentAttachment attachment2 = new CurrencyDocumentAttachment();
		attachment2.setId(2222222);
		attachment2.setFilename("attachment2.pdf");
		try {
			attachment2.setGOSTDigest(HashUtils.computeGOSTHash(SECOND_ATTACHMENT_CONTENT.getBytes()));
			attachment2.setSHA1Digest(HashUtils.computeHash(SECOND_ATTACHMENT_CONTENT.getBytes(), "SHA1"));
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		} catch (NoSuchProviderException e) {
			throw new RuntimeException(e);
		}
		attachments.add(attachment2);
		tbsInfo.setAttachements(attachments);
		loadDataToSign(tbsInfo);
	}

	private void initScripts() {

		JavaScript.getCurrent().addFunction("ru.alfabank.orrpp.jscallback.setPluginInstalled",
				new JavaScriptFunction() {

					private static final long serialVersionUID = 1L;

					@Override
					public void call(JSONArray arguments) throws JSONException {
						isPluginInstalled = arguments.getBoolean(0);
						if (!isPluginInstalled) {
							Notification.show("Ошибка", "Не установлен плагин Крипто-Про!", Type.ERROR_MESSAGE);
						}
					}
				});
		JavaScript.getCurrent().addFunction("ru.alfabank.orrpp.jscallback.setCMS", new JavaScriptFunction() {

			private static final long serialVersionUID = 1L;

			@Override
			public void call(JSONArray arguments) throws JSONException {
				VaadinSession session = getUI().getSession();

				boolean isAttachment = false;
				int id = 0;
				String base64EncodedCMS = arguments.getString(0);
				if (arguments.length() > 1) {
					isAttachment = arguments.getBoolean(1);
					id = arguments.getInt(2);
				}

				byte[] requestText = (byte[]) session.getAttribute("requesttext");

				addSignature(base64EncodedCMS, id, isAttachment, requestText);
			}
		});

		JavaScript.getCurrent().addFunction("ru.alfabank.orrpp.jscallback.send", new JavaScriptFunction() {

			private static final long serialVersionUID = 1L;

			@Override
			public void call(JSONArray arguments) throws JSONException {
				LOGGER.info("Sending request to processing. id: " + arguments.getString(0));
				close();
				Notification.show("Успешно", Type.HUMANIZED_MESSAGE);
			}
		});

		JavaScript.getCurrent().addFunction("ru.alfabank.orrpp.jscallback.showError", new JavaScriptFunction() {

			private static final long serialVersionUID = 1L;

			@Override
			public void call(JSONArray arguments) throws JSONException {

				Notification.show("Ошибка при подписании", "\n" + arguments.getString(0), Type.ERROR_MESSAGE);
			}
		});

		JavaScript.getCurrent().addFunction("ru.alfabank.orrpp.jscallback.log", new JavaScriptFunction() {

			private static final long serialVersionUID = 1L;

			@Override
			public void call(JSONArray arguments) throws JSONException {
				LOGGER.info("\t*****\tSIGNING DEBUG\t*****\t\t" + arguments.getString(0));
			}
		});

	}

	protected void addSignature(String base64EncodedCMS, int id, boolean isAttachment, byte[] requestText) {
		// throw new NotImplementedException("addSignature");

		byte[] encodedBytes = Base64.decode(base64EncodedCMS);

		System.out.println("Recieved bytes: " + Bytes.renderBytes(encodedBytes));

		try {
			Files.write(encodedBytes, new File("W:\\tmp\\" + id + (isAttachment ? "_attachment" : "_request") + "_debug_contentinfo.p7s"));
		} catch (IOException e1) {
			throw new RuntimeException(e1);
		}

		// ASN1ObjectIdentifier oid2 = new
		// ASN1ObjectIdentifier("1.2.840.113549.1.9.16.2.47");
		// ASN1Type type = PKCSRegistry.getDefaultRegistry().getASN1Type(oid2);
		// if (type == null) {
		// System.err.println("Cannot load type for OID: " + oid2);
		// }

		ContentInfo ci = new ContentInfo();
		try {
			ASN1Util.decode(encodedBytes, ci);
		} catch (ASN1DecodeException e) {
			// TODO уточнить обработку исключения
			throw new RuntimeException(e);
		}
		SignedData sd = (SignedData) ci.getContent();

	}

	private void initSkeleton() {
		// 1 lvl
		mainLayout = new VerticalLayout();
		mainLayout.setId("signForm");
		setContent(mainLayout);

		CustomLayout signLayout2;
		signLayout2 = new CustomLayout("signPageLayout3");
		// signLayout2 = new CustomLayout("sign");
		signLayout2.setSizeFull();

		// 2 lvl
		VerticalLayout signLayout = new VerticalLayout();
		signLayout.setSizeFull();
		signLayout.setMargin(true);
		signLayout.setSpacing(true);
		signLayout.addComponent(signLayout2);

		text = new TextArea();
		text.setId("tbsText");
		text.setWidth("100%");
		text.setHeight("200px");
		text.setReadOnly(true);

		signLayout2.addComponent(text, "text");

		Panel attachmentPanel = new Panel();
		attachmentPanel.setStyleName("light");
		attachmentPanel.setWidth("100%");
		attachmentPanel.setHeight("150px");
		attachmentsLayout = new VerticalLayout();
		attachmentsLayout.setSpacing(true);
		attachmentsLayout.setWidth("100%");
		attachmentPanel.setContent(attachmentsLayout);
		signLayout2.addComponent(attachmentPanel, "files");

		VerticalLayout hashLayout = new VerticalLayout();
		hashLayout.setSpacing(true);
		hashLayout.setSizeFull();
		signLayout2.addComponent(hashLayout, "hash");

		HorizontalLayout requestHashLayout = new HorizontalLayout();
		requestHashLayout.setSpacing(true);
		hashLayout.addComponent(requestHashLayout);
		String sha1Hash = "";
		requestHashLayout.addComponent(new Label(""));
		requestHashLabel = new Label(sha1Hash, ContentMode.HTML);
		requestHashLayout.addComponent(requestHashLabel);

		HorizontalLayout resultLayout = new HorizontalLayout();
		resultLayout.setSizeFull();
		resultLayout.setMargin(true);
		resultLayout.setSpacing(true);
		resultLayout.setVisible(debug);

		mainLayout.addComponent(signLayout);
		mainLayout.setExpandRatio(signLayout, 1.0f);

		mainLayout.addComponent(resultLayout);
		mainLayout.setExpandRatio(resultLayout, 0.2f);

		// 3 lvl
		// TBS
		VerticalLayout toBeSignedLayout = new VerticalLayout();
		toBeSignedLayout.setSizeFull();
		HorizontalLayout signControlsLayout = new HorizontalLayout();
		signControlsLayout.setSpacing(true);
		signControlsLayout.setSizeFull();

	}

	private void initForm() {
		setModal(true);
		setClosable(true);
		setCloseShortcut(KeyCode.ESCAPE);
		setResizable(false);
		setHeight("600px");
		setWidth("1000px");
		setCaption("Подписание документов");
		center();
	}

	public void loadDataToSign(TBSInfo tbsInfo) {

		attachmentsLayout.removeAllComponents();

		List<AttachmentInfo> atts = new ArrayList<AttachmentInfo>();

		if (tbsInfo != null) {

			for (final CurrencyDocumentAttachment attachmentAnnotation : tbsInfo.getAttachements()) {
				atts.add(new AttachmentInfo(attachmentAnnotation.getId(),
						Bytes.renderBytes(attachmentAnnotation.getGOSTDigest()).replace(" ", "")));
				HorizontalLayout attachmentLayout = new HorizontalLayout();
				attachmentLayout.setSpacing(true);
				attachmentsLayout.addComponent(attachmentLayout);
				Button download = new Button(StringUtils.trimStringToLength(attachmentAnnotation.getFilename(), 50));
				download.setStyleName(Reindeer.BUTTON_LINK);
				attachmentLayout.addComponent(download);

				Label digestLabel = new Label(
						"<small>SHA1: " + Bytes.renderBytes(attachmentAnnotation.getSHA1Digest()).replace(" ", "")
								+ " ГОСТ Р 3411-94: "
								+ Bytes.renderBytes(attachmentAnnotation.getGOSTDigest()).replace(" ", "") + "</small>",
						ContentMode.HTML);
				attachmentLayout.addComponent(digestLabel);

				StreamResource fileResource = createDownloadResource(attachmentAnnotation);
				FileDownloader fileDownloader = new FileDownloader(fileResource);
				fileDownloader.extend(download);
			}
		}

		// Получили данные для подписания
		byte[] requestText = tbsInfo.getTbsText().getBytes();
		LOGGER.log(Level.FINER,
				"To be signed text:" + new String(requestText) + ", bytes: " + Bytes.renderBytes(requestText));
		// TODO подумать как лучше передавать текст реквеста.
		VaadinSession session = VaadinSession.getCurrent();
		// text.setValue(new String(requestText));
		session.setAttribute("requesttext", requestText);
		String requestGostHash;
		try {
			requestGostHash = Bytes.renderBytes(HashUtils.computeGOSTHash(requestText)).replace(" ", "");
			String requestSHA1Hash = Bytes.renderBytes(HashUtils.computeHash(requestText, "SHA1")).replace(" ", "");
			requestHashLabel
					.setValue("<small>SHA1: " + requestSHA1Hash + " ГОСТ Р 3411-94: " + requestGostHash + "</small>");
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
			// TODO уточнить обработку исключения
			throw new RuntimeException(e);
		}
		try {
			String prepareDataCall = generatePrepareDataCall(tbsInfo, atts, requestGostHash);
			LOGGER.log(Level.FINEST, "Generated prepareDataCall: " + prepareDataCall);
			JavaScript.getCurrent().execute(prepareDataCall);
		} catch (JSONException e) {
			// TODO уточнить обработку исключения
			throw new RuntimeException(e);
		}
	}

	private StreamResource createDownloadResource(final CurrencyDocumentAttachment attachment) {
		StreamResource resource = null;
		try {
			resource = new StreamResource(new StreamSource() {

				private static final long serialVersionUID = 1L;

				@Override
				public InputStream getStream() {
					InputStream inStream = null;
					return inStream;
				}
			}, URLEncoder.encode(attachment.getFilename(), "UTF-8"));
			resource.setCacheTime(0);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
		return resource;
	}

	private static String generatePrepareDataCall(TBSInfo tbsInfo, List<AttachmentInfo> atts, String gostHash)
			throws JSONException {
		JSONObject params = new JSONObject();
		params.put("requestId", tbsInfo.getIdState());
		params.put("requestText", tbsInfo.getTbsText());
		params.put("requestTextHash", gostHash);

		JSONArray mJSONArray = new JSONArray();

		for (AttachmentInfo ai : atts) {
			JSONObject aiObj = new JSONObject();
			aiObj.put("id", ai.id);
			aiObj.put("hash", ai.hash);
			mJSONArray.put(aiObj);
		}
		params.put("attachmentsHashes", mJSONArray);

		String call = "prepareData(" + params + ")";
		return call;
	}

}
