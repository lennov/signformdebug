/**
 * Скрипт для подписывания данных с помощью плагина крипто-про
 *
 * Last updated: 28.03.2016 Version 4.0
 *
 * Исправлено: в IE теперь список сертификатов грузится с первого раза
 *
 */
var constants = {
    CADESCOM_CADES_BES: 1,
    CAPICOM_CURRENT_USER_STORE: 2,
    CAPICOM_LOCAL_MACHINE_STORE: 1,
    CAPICOM_MY_STORE: "My",
    CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED: 2,
    CADESCOM_BASE64_TO_BINARY: 1,
    CAPICOM_CERTIFICATE_FIND_SHA1_HASH: 0,
    CERT_CHOOSER: "certChooser",
    CERTIFICATES_OPTION_GROUP: "certificatesOptionGroup",
    CAPICOM_CERT_INFO_SUBJECT_SIMPLE_NAME: 0,
    CAPICOM_CERT_INFO_ISSUER_SIMPLE_NAME: 1,
    CADESCOM_HASH_ALGORITHM_CP_GOST_3411: 100,
    PROVIDER_NAME: "Crypto-Pro GOST R 34.10-2001 Cryptographic Service Provider",
    PROVIDER_TYPE: 75,
    CADES_OBJECT_ID: "cadesplugin",
    CERT_TABLE_BODY: "certTableBody",
    TABLE_CONTENT: "certTableContent",
    SUBJECT_COLUMN_WIDTH: 200,
    ISSUER_COLUMN_WIDTH: 230,
    VALIDDATE_COLUMN_WIDTH: 120,
    SIGN_BUTTON_ID: "signButton"
};

var requestTextHash;
var requestText;
var currentDate;

var oddRow = false;

/**
 * Метод выполняется при инициализации формы, через callback дает приложению
 * значть установлен ли плагин Крипто-Про
 */
function init() {
    log("Initialization started...");
    var pluginInstalled = isPluginInstalled();
    if (!pluginInstalled) {
        log("Plugin CryptoPro not found! Initialization failed.");
        var signButton = document.getElementById(constants.SIGN_BUTTON_ID);
        signButton.disabled = true;//Блокировка кнопки
        signButton.className = "v-button v-disabled"; 
        return;
    } else {
        log("Initialization done.");
    }

    var version = getVersion(constants.PROVIDER_NAME, constants.PROVIDER_TYPE);
    log("CSP version: " + version.toString());
    if ((version.MajorVersion < 3) || ((version.MajorVersion >= 3) && (version.MinorVersion < 6))) {
        log("CSP version: " + version.toString() + " is unsupported!");
        pluginInstalled = false;
    }
    pluginInstalled = preparePluginObject();

    ru.alfabank.orrpp.jscallback.setPluginInstalled(pluginInstalled);

    setSignButtonEnabled(false);
}

/**
 * Проверка браузера
 *
 * @returns {Boolean}
 */
function isIE() {
    return (("Microsoft Internet Explorer" == navigator.appName) || // IE < 11
    (null != navigator.userAgent.match(/Trident\/./i)));// IE11
}




/**
 * Возвращает true, если плагин установлен, или false в противном случае.
 *
 * @returns {Boolean}
 */
function isPluginInstalled() {
    if (isIE()) {
        try {
            var obj = new ActiveXObject("CAdESCOM.CPSigner");
            return true;
        } catch (err) {
        }
    } else {
        var mimetype = navigator.mimeTypes["application/x-cades"];
        if (mimetype) {
            var plugin = mimetype.enabledPlugin;
            if (plugin) {
                return true;
            }
        }
    }
    return false;
}

function getVersion(ProviderName, ProviderType) {
    var oVersion;
    try {
        var oAbout = createObject("CAdESCOM.About");
        oVersion = oAbout.CSPVersion(ProviderName, parseInt(ProviderType, 10));
        return oVersion;
    }
    catch (er) {
        if (er.message.indexOf("0x80090019") + 1) {
            return "Указанный CSP не установлен";
        }
        else {
            return er.message;
        }
        return false;
    }
}

/**
 * Хелпер для вывода ошибок
 *
 * @param e
 * @returns
 */
function getErrorMessage(e) {
    var err = e.message;
    if (!err) {
        err = e;
    } else if (e.number) {
        err += " (" + e.number + ")";
    }
    return err;
}

/**
 * Создает инстанс плагина Крипто-Про
 *
 * @param name
 * @returns {ActiveXObject}
 */
function createObject(name) {
    switch (navigator.appName) {
        case "Microsoft Internet Explorer":
            return new ActiveXObject(name);
        default:
            var userAgent = navigator.userAgent;
            if (userAgent.match(/Trident\/./i)) {// IE11
                return new ActiveXObject(name);
            }
            var cadesobject = document.getElementById(constants.CADES_OBJECT_ID);
            return cadesobject.CreateObject(name);
    }
}

/**
 * Запуск подписания. Вызывается из формы выбора прватного ключа
 */
function runSigning(sCertHash) {
    
    var call = document.getElementById('preparedCall');
    prepareData(JSON.parse(call.value));
    
    var ok = true;

    log("Signing started...");

    if ("" == sCertHash) {
	logerror("Не выбран сертификат");
	return;
    }

    if (requestTextHash != undefined) {
	log("Signing registry hash running :" + requestTextHash);
	try {
	    var signedMessage = signCreate(sCertHash, requestTextHash);
	    if (signedMessage != null) {
		ru.alfabank.orrpp.jscallback.setCMS(signedMessage);
		log("Signing registry text done!");
	    } else {
		ok = false;
	    }
	} catch (e) {
	    ok = false;
	    logerror(e.message);
	    return;
	}
	if(!ok){
	    logerror("Signing failed!");
	}
    } else {
	log("Signing registry skipped, because nothing to sign!");
    }
}

function initializeHashedData(hashAlg, sHashValue) {
    // Создаем объект CAdESCOM.HashedData
    var oHashedData = createObject("CAdESCOM.HashedData");
    // Инициализируем объект заранее вычисленным хэш-значением
    // Алгоритм хэширования нужно указать до того, как будет передано
    // хэш-значение
    oHashedData.Algorithm = hashAlg;
    oHashedData.SetHashValue(sHashValue);
    return oHashedData;
}

/**
 * Подписывает и возвращает CMS.
 *
 * TODO удалить сертификаты из CMS
 *
 * @param certSHA1Hash -
 *                хэш сертификата, для поиска в хранилище
 * @param dataToSign -
 *                данные, закодированные в base64.
 * @returns CMS(ContentInfo)
 */
function signCreate(certSHA1Hash, hashToSign) {

    // Алгоритм хэширования, при помощи которого было вычислено хэш-значение
    // Полный список поддерживаемых алгоритмов указан в перечислении
    // CADESCOM_HASH_ALGORITHM
    var hashAlg = constants.CADESCOM_HASH_ALGORITHM_CP_GOST_3411; // ГОСТ Р 34.11-94

    // Создаем объект CAdESCOM.HashedData
    var oHashedData = initializeHashedData(hashAlg, hashToSign);

    var oStore = createObject("CAPICOM.Store");
    oStore.Open(constants.CAPICOM_CURRENT_USER_STORE, constants.CAPICOM_MY_STORE, constants.CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

    var oCertificates = oStore.Certificates.Find(constants.CAPICOM_CERTIFICATE_FIND_SHA1_HASH, certSHA1Hash);
    if (oCertificates.Count == 0) {
        logerror("Certificate not found: " + certSHA1Hash);
        return;
    }
    var oCertificate = oCertificates.Item(1);

    var oSigner = createObject("CAdESCOM.CPSigner");
    oSigner.Certificate = oCertificate;
    var oSignedData = createObject("CAdESCOM.CadesSignedData");

    oSignedData.ContentEncoding = constants.CADESCOM_BASE64_TO_BINARY;
    //oSignedData.Content = Base64.encode(requestText);

    var sSignedMessage = "";
    try {
        sSignedMessage = oSignedData.SignHash(oHashedData, oSigner, constants.CADESCOM_CADES_BES);
	//sSignedMessage = oSignedData.SignCades(oSigner, constants.CADESCOM_CADES_BES, true);
    } catch (err) {
        handleError(err.message);
        return null;
    }

    oStore.Close();
    return sSignedMessage;
}

/**
 * Закрывает окно выбора сертификатов
 */
function closeChooser() {
    signForm = document.getElementById('signForm');
    if (signForm != null) {
        chooser = document.getElementById(constants.CERT_CHOOSER);
        if (chooser != null) {
            signForm.removeChild(chooser);
        } else {
            log("Cannot find chooser form!");
        }
    } else {
        log("Cannot find sign form!");
    }
}

function prepareData(params) {
    setSignButtonEnabled(false);
    if(params==null){
	requestText = "";
	requestTextHash="";
	currentDate = undefined;
    }else{    
	requestTextHash = params.requestTextHash;
	requestText = params.requestText;
	currentDate = new Date(params.currentDate);
	setSignButtonEnabled(true);
    }
    var tbsText = document.getElementById("tbsText");
    tbsText.value = requestText;
    
    
}

function setSignButtonEnabled(enabled){
    var signButton = document.getElementById(constants.SIGN_BUTTON_ID);
    signButton.disable = !enabled;
    signButton.className = (enabled ? "v-button": "v-button v-disabled");
    signButton.onclick= (!enabled ? undefined : function(){showCertChooser();});
}

/**
 * Хэлпер для вывода сообщений
 *
 * @param message
 */
function handleError(message) {
    log(message);
    ru.alfabank.orrpp.jscallback.showError(message);
}

/**
 * Заполнение списка сертификатов, подходящих для подписания
 */
function fillCertificatesList() {
    log("Collecting certificates...");

    var certificates1 = null;
    var certificates2 = null;

    var oStore = createObject("CAPICOM.Store");
    try {
        oStore.Open(constants.CAPICOM_CURRENT_USER_STORE, constants.CAPICOM_MY_STORE, constants.CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);
        certificates1 = oStore.Certificates;
        oStore.Close();
    } catch (err) {
        logerror("Error during reading sertificates from current user store. Error: " + getErrorMessage(err), "debug");
    }

    /**
     * Просмотр сертификатов локальной машны заканчивается обычно эксепшном, НО(!!!) без этого в IE до открытия консоли вообще не возвращается список сертификатов!
     */
    oStore = createObject("CAPICOM.Store");
    try {
        oStore.Open(constants.CAPICOM_LOCAL_MACHINE_STORE, constants.CAPICOM_MY_STORE, constants.CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);
        certificates2 = oStore.Certificates;
        oStore.Close();
    } catch (err) {
        log("Error during reading sertificates from current machine store. Error: " + getErrorMessage(err), "debug");
    }

    var certCount = 0;
    var oCertificates = null;
    if (certificates1 != null) {
        certCount += certificates1.Count;
        oCertificates = certificates1;
    }
    if (certificates2 != null) {
        certCount += certificates2.Count;
        if (oCertificates != null) {

            for (var i = 1; i <= certificates2.Count; i++) {
                oCertificates.add(certificates2.Item(i));
            }

        } else {
            oCertificates = certificates2;
        }
    }
    log("Found " + certCount + " certificates.");

    //Увеличиваю высоту для контента таблицы

    var tcontent = document.getElementById(constants.TABLE_CONTENT);
    tcontent.style.height = (20 * certCount) + "px";


    if (certCount != 0) {
        for (var i = 1; i <= certCount; i++) {
            var cert = oCertificates.Item(i);
            var validFromDate = cert.ValidFromDate;
            var validToDate = cert.ValidToDate;
            if (isFinite(currentDate) && isFinite(validFromDate) && isFinite(validToDate) && (validFromDate <= currentDate && currentDate <= validToDate)){
            	var hash = cert.Thumbprint;
            	var subjectSimpleName = cert.GetInfo(constants.CAPICOM_CERT_INFO_SUBJECT_SIMPLE_NAME);
                var issuerSimpleName = cert.GetInfo(constants.CAPICOM_CERT_INFO_ISSUER_SIMPLE_NAME);
                var isValid = cert.IsValid().Result;
                addCertificateOption(hash, subjectSimpleName, issuerSimpleName, validFromDate, validToDate, isValid);	
            }            
        }
    } else {
        var signButton = document.getElementById("signButton");
        signButton.style.visibility = "hidden";
    }
    log("Collecting certificates done.");
}

function showCertChooser() {

    var certChooser = document.createElement("div");
    certChooser.setAttribute("id", constants.CERT_CHOOSER);
    certChooser.setAttribute("style", "overflow: visible; top: 10%;  left: 10%; height: 80%;  width: 80%; position: absolute; padding-top: 36px; padding-bottom: 9px;z-index: 20000;");
    certChooser.setAttribute("class", "v-panel v-widget v-has-width v-has-height");


    //Caption
    var pcaptionwrap = document.createElement("div");
    pcaptionwrap.setAttribute("style", "margin-top: -36px;");
    pcaptionwrap.setAttribute("class", "v-panel-captionwrap");
    certChooser.appendChild(pcaptionwrap);

    var pcaption = document.createElement("div");
    pcaption.setAttribute("class", "v-panel-caption");
    pcaptionwrap.appendChild(pcaption);

    var pcaptionspan = document.createElement("span");
    pcaptionspan.textContent = "Выбор ключа для подписания";
    pcaption.appendChild(pcaptionspan);

    //Content

    var pcontent = document.createElement("div");
    pcontent.setAttribute("class", "v-panel-content v-scrollable");
    pcontent.setAttribute("tabindex", "-1");
    pcontent.setAttribute("style", "position: relative;");
    certChooser.appendChild(pcontent);

    var mainvl = document.createElement("div");
    mainvl.setAttribute("style", "height: 100%; width: 100%;");
    mainvl.setAttribute("class", "v-verticallayout v-layout v-vertical v-widget v-has-width v-has-height v-margin-top v-margin-right v-margin-bottom v-margin-left");
    pcontent.appendChild(mainvl);

    var mainvlexp = document.createElement("div");
    mainvlexp.setAttribute("style", "padding-top: 7px;");
    mainvlexp.setAttribute("class", "v-expand");
    mainvl.appendChild(mainvlexp);

    var mainvlexpslot = document.createElement("div");
    mainvlexpslot.setAttribute("style", "height: 90%; margin-top: -7px;");
    mainvlexpslot.setAttribute("class", "v-slot");
    mainvlexp.appendChild(mainvlexpslot);

    var tvl = document.createElement("div");
    tvl.setAttribute("style", "height: 100%; width: 100%;");
    tvl.setAttribute("class", "v-verticallayout v-layout v-vertical v-widget v-has-width v-has-height");
    mainvlexpslot.appendChild(tvl);

    var tvlexp = document.createElement("div");
    tvlexp.setAttribute("style", "padding-top: 0px;");
    tvlexp.setAttribute("class", "v-expand");
    tvl.appendChild(tvlexp);

    var tvlexpslot = document.createElement("div");
    tvlexpslot.setAttribute("style", "height: 100%; margin-top: 0px;");
    tvlexpslot.setAttribute("class", "v-slot");
    tvlexp.appendChild(tvlexpslot);

    var table = document.createElement("div");
    table.setAttribute("style", "height: 100%; width: 100%;");
    table.setAttribute("class", "v-table v-widget v-has-width v-has-height");
    tvlexpslot.appendChild(table);

    var tableheadwrap = document.createElement("div");
    tableheadwrap.setAttribute("style", "width: 758px;");
    tableheadwrap.setAttribute("class", "v-table-header-wrap");
    tableheadwrap.setAttribute("aria-hidden", "false");
    table.appendChild(tableheadwrap);

    var tablehead = document.createElement("div");
    tablehead.setAttribute("style", "overflow: hidden;");
    tablehead.setAttribute("class", "v-table-header");
    tableheadwrap.appendChild(tablehead);

    var tablehead1 = document.createElement("div");
    tablehead1.setAttribute("style", "width: 900000px;");
    tablehead.appendChild(tablehead1);

    var tabletable = document.createElement("table");
    tablehead1.appendChild(tabletable);

    var tabletablebody = document.createElement("tbody");
    tabletable.appendChild(tabletablebody);

    var tabletablebodytr = document.createElement("tr");
    tabletablebody.appendChild(tabletablebodytr);

    //Subject
    var tdsubject = document.createElement("td");
    tdsubject.style.width = constants.SUBJECT_COLUMN_WIDTH + 13 + "px";
    tdsubject.setAttribute("class", "v-table-header-cell");
    tabletablebodytr.appendChild(tdsubject);

    var tdsubjectr = document.createElement("div");
    tdsubjectr.setAttribute("class", "v-table-resizer");
    tdsubject.appendChild(tdsubjectr);

    var tdsubjectsi = document.createElement("div");
    tdsubjectsi.setAttribute("class", "v-table-sort-indicator");
    tdsubject.appendChild(tdsubjectsi);

    var tdsubjectcaption = document.createElement("div");
    tdsubjectcaption.style.width = constants.SUBJECT_COLUMN_WIDTH - 5 + "px";
    tdsubjectcaption.setAttribute("class", "v-table-caption-container v-table-caption-container-align-left");
    tdsubjectcaption.textContent = "Выдан кому";
    tdsubject.appendChild(tdsubjectcaption);

    //Issuer
    var tdissuer = document.createElement("td");
    tdissuer.style.width = constants.ISSUER_COLUMN_WIDTH + 13 + "px";
    tdissuer.setAttribute("class", "v-table-header-cell");
    tabletablebodytr.appendChild(tdissuer);

    var tdissuerr = document.createElement("div");
    tdissuerr.setAttribute("class", "v-table-resizer");
    tdissuer.appendChild(tdissuerr);

    var tdissuercaption = document.createElement("div");
    tdissuercaption.style.width = constants.ISSUER_COLUMN_WIDTH - 5 + "px";
    tdissuercaption.setAttribute("class", "v-table-caption-container v-table-caption-container-align-left");
    tdissuercaption.textContent = "Выдан кем";
    tdissuer.appendChild(tdissuercaption);

    //ValidFrom
    var tdvfrom = document.createElement("td");
    tdvfrom.style.width = constants.VALIDDATE_COLUMN_WIDTH + 13 + "px";
    tdvfrom.setAttribute("class", "v-table-header-cell");
    tabletablebodytr.appendChild(tdvfrom);

    var tdvfromr = document.createElement("div");
    tdvfromr.setAttribute("class", "v-table-resizer");
    tdvfrom.appendChild(tdvfromr);

    var tdvfromsi = document.createElement("div");
    tdvfromsi.setAttribute("class", "v-table-sort-indicator");
    tdvfrom.appendChild(tdvfromsi);

    var tdvfromcaption = document.createElement("div");
    tdvfromcaption.style.width = constants.VALIDDATE_COLUMN_WIDTH - 5 + "px";
    tdvfromcaption.setAttribute("class", "v-table-caption-container v-table-caption-container-align-left");
    tdvfromcaption.textContent = "Действителен с";
    tdvfrom.appendChild(tdvfromcaption);

    //ValidTo
    var tdvto = document.createElement("td");
    tdvto.style.width = constants.VALIDDATE_COLUMN_WIDTH + 13 + "px";
    tdvto.setAttribute("class", "v-table-header-cell");
    tabletablebodytr.appendChild(tdvto);

    var tdvtor = document.createElement("div");
    tdvtor.setAttribute("class", "v-table-resizer");
    tdvto.appendChild(tdvtor);

    var tdvtosi = document.createElement("div");
    tdvtosi.setAttribute("class", "v-table-sort-indicator");
    tdvto.appendChild(tdvtosi);

    var tdvtocaption = document.createElement("div");
    tdvtocaption.style.width = constants.VALIDDATE_COLUMN_WIDTH - 5 + "px";
    tdvtocaption.setAttribute("class", "v-table-caption-container v-table-caption-container-align-left");
    tdvtocaption.textContent = "Действителен по";
    tdvto.appendChild(tdvtocaption);


    //Table body
    var tablebodywrap = document.createElement("div");
    tablebodywrap.setAttribute("style", "zoom: 1; position: relative; overflow: auto; height: 278px; width: 758px;");
    tablebodywrap.setAttribute("class", "v-scrollable v-table-body-wrapper v-table-body");
    tablebodywrap.setAttribute("tabindex", "-1");
    table.appendChild(tablebodywrap);

    var tablebodywrapns = document.createElement("div");
    tablebodywrapns.setAttribute("id", constants.TABLE_CONTENT);
    tablebodywrapns.setAttribute("style", "height: 20px;");
    tablebodywrapns.setAttribute("class", "v-table-body-noselection");
    tablebodywrap.appendChild(tablebodywrapns);

    var tablebodywrapnsrs = document.createElement("div");
    tablebodywrapnsrs.setAttribute("style", "height: 0px;");
    tablebodywrapnsrs.setAttribute("class", "v-table-row-spacer");
    tablebodywrapns.appendChild(tablebodywrapnsrs);

    var bodyTable = document.createElement("table");
    bodyTable.setAttribute("class", "v-table-table");
    tablebodywrapns.appendChild(bodyTable);

    var bodyTableBody = document.createElement("tbody");
    bodyTableBody.setAttribute("id", constants.CERT_TABLE_BODY);///Вот сюда насыпать будем строки сертов
    bodyTableBody.setAttribute("class", "v-table-table");
    bodyTable.appendChild(bodyTableBody);

    var rs = document.createElement("div");
    rs.setAttribute("style", "height: 0px;");
    rs.setAttribute("class", "v-table-row-spacer");
    tablebodywrapns.appendChild(rs);

    var tablebodywrap1 = document.createElement("div");
    tablebodywrap1.setAttribute("style", "position: fixed; top: 0px; left: 0px;");
    tablebodywrap1.setAttribute("tabindex", "0");
    tablebodywrap.appendChild(tablebodywrap1);

    //tablebodywrap closed

    var tablefootwrap = document.createElement("div");
    tablefootwrap.setAttribute("style", "display: none; width: 758px;");
    tablefootwrap.setAttribute("class", "v-table-footer-wrap");
    tablefootwrap.setAttribute("aria-hidden", "true");
    table.appendChild(tablefootwrap);

    var footer = document.createElement("div");
    footer.setAttribute("style", "overflow: hidden;");
    footer.setAttribute("class", "v-table-footer");
    tablefootwrap.appendChild(footer);

    var footer1 = document.createElement("div");
    footer1.setAttribute("style", "width: 900000px;");
    footer.appendChild(footer1);

    var ftable = document.createElement("table");
    footer1.appendChild(ftable);

    var ftablebody = document.createElement("tbody");
    ftable.appendChild(ftablebody);

    var ftablebodytr = document.createElement("tr");
    ftablebody.appendChild(ftablebodytr);

    //footer td1
    var footertd1 = document.createElement("td");
    footertd1.setAttribute("style", "width: 183px;");
    ftablebodytr.appendChild(footertd1);

    var footertd1div = document.createElement("div");
    footertd1div.setAttribute("style", "width: 170px;");
    footertd1div.setAttribute("class", "v-table-footer-container");
    footertd1div.textContent = "&nbsp";
    footertd1.appendChild(footertd1div);

    //footer td2
    var footertd2 = document.createElement("td");
    footertd2.setAttribute("style", "width: 162px;");
    ftablebodytr.appendChild(footertd2);

    var footertd2div = document.createElement("div");
    footertd2div.setAttribute("style", "width: 149px;");
    footertd2div.setAttribute("class", "v-table-footer-container");
    footertd2div.textContent = "&nbsp";
    footertd2.appendChild(footertd2div);

    //footer td3
    var footertd3 = document.createElement("td");
    footertd3.setAttribute("style", "width: 216px;");
    ftablebodytr.appendChild(footertd3);

    var footertd3div = document.createElement("div");
    footertd3div.setAttribute("style", "width: 203px;");
    footertd3div.setAttribute("class", "v-table-footer-container");
    footertd3div.textContent = "&nbsp";
    footertd3.appendChild(footertd3div);

    //footer td4
    var footertd4 = document.createElement("td");
    footertd4.setAttribute("style", "width: 176px;");
    ftablebodytr.appendChild(footertd4);

    var footertd4div = document.createElement("div");
    footertd4div.setAttribute("style", "width: 163px;");
    footertd4div.setAttribute("class", "v-table-footer-container");
    footertd4div.textContent = "&nbsp";
    footertd4.appendChild(footertd4div);

    var spacing = document.createElement("div");
    spacing.setAttribute("class", "v-spacing");
    mainvlexp.appendChild(spacing);

    var btnSlot = document.createElement("div");
    btnSlot.setAttribute("style", "height: 10%;");
    btnSlot.setAttribute("class", "v-slot");
    mainvlexp.appendChild(btnSlot);

    var btnLayout = document.createElement("div");
    btnLayout.setAttribute("style", "height: 100%; width: 100%;");
    btnLayout.setAttribute("class", "v-verticallayout v-layout v-vertical v-widget v-has-width v-has-height");
    btnSlot.appendChild(btnLayout);

    var btnLayoutExp = document.createElement("div");
    btnLayoutExp.setAttribute("style", "padding-top: 0px;");
    btnLayoutExp.setAttribute("class", "v-expand");
    btnLayout.appendChild(btnLayoutExp);

    var btnLayoutExpSlot = document.createElement("div");
    btnLayoutExpSlot.setAttribute("style", "height: 100%; margin-top: 0px;");
    btnLayoutExpSlot.setAttribute("class", "v-slot v-align-center v-align-middle");
    btnLayoutExp.appendChild(btnLayoutExpSlot);

    var btnLayoutExpSlotDiv = document.createElement("div");
    btnLayoutExpSlotDiv.setAttribute("role", "button");
    btnLayoutExpSlotDiv.setAttribute("class", "v-button v-widget");
    btnLayoutExpSlotDiv.setAttribute("tabindex", "0");
    btnLayoutExpSlot.appendChild(btnLayoutExpSlotDiv);
    btnLayoutExpSlotDiv.addEventListener("mouseup", function () {
        closeChooser();
    }, false);

    var btnLayoutExpSlotDivSpan = document.createElement("span");
    btnLayoutExpSlotDivSpan.setAttribute("class", "v-button-wrap");
    btnLayoutExpSlotDiv.appendChild(btnLayoutExpSlotDivSpan);

    var btnLayoutExpSlotDivSpanSpan = document.createElement("span");
    btnLayoutExpSlotDivSpanSpan.setAttribute("class", "v-button-caption");
    btnLayoutExpSlotDivSpan.appendChild(btnLayoutExpSlotDivSpanSpan);
    btnLayoutExpSlotDivSpanSpan.textContent = "Отмена";

    var decor = document.createElement("div");
    decor.setAttribute("style", "margin-bottom: -9px;");
    decor.setAttribute("class", "v-panel-deco");
    certChooser.appendChild(decor);

    var d = document.getElementById('signForm');
    d.appendChild(certChooser);

    fillCertificatesList();

}

function addCertificateOption(hash, subjectSimpleName, issuerSimpleName, validFromDate, validToDate, isValid) {

    var tbody = document.getElementById(constants.CERT_TABLE_BODY);

    var tr = document.createElement("tr");
    if (oddRow) {
        tr.setAttribute("class", "v-table-row");
    } else {
        tr.setAttribute("class", "v-table-row-odd");
    }
    oddRow = !oddRow;
    tbody.appendChild(tr);

    var td = document.createElement("td");
    td.setAttribute("style", "cursor: pointer; color: rgb(27, 105, 159);");
    td.style.width = constants.SUBJECT_COLUMN_WIDTH + "px";
    td.setAttribute("class", "v-table-cell-content");
    tr.appendChild(td);

    var tddiv = document.createElement("div");
    tddiv.style.width = constants.SUBJECT_COLUMN_WIDTH + "px";
    tddiv.setAttribute("class", "v-table-cell-wrapper");
    tddiv.textContent = subjectSimpleName;
    td.appendChild(tddiv);


    var td1 = document.createElement("td");
    td1.setAttribute("style", "cursor: pointer; color: rgb(27, 105, 159);");
    td1.style.width = constants.ISSUER_COLUMN_WIDTH + "px";
    td1.setAttribute("class", "v-table-cell-content");
    tr.appendChild(td1);

    var tddiv1 = document.createElement("div");
    tddiv1.style.width = constants.ISSUER_COLUMN_WIDTH + "px";
    tddiv1.setAttribute("class", "v-table-cell-wrapper");
    tddiv1.textContent = issuerSimpleName;
    td1.appendChild(tddiv1);


    var td2 = document.createElement("td");
    td2.setAttribute("style", "cursor: pointer; color: rgb(27, 105, 159);");
    td2.style.width = constants.VALIDDATE_COLUMN_WIDTH + "px";
    td2.setAttribute("class", "v-table-cell-content");
    tr.appendChild(td2);

    var tddiv2 = document.createElement("div");
    tddiv2.style.width = constants.VALIDDATE_COLUMN_WIDTH + "px";
    tddiv2.setAttribute("class", "v-table-cell-wrapper");
    tddiv2.textContent = formatDate(validFromDate);
    td2.appendChild(tddiv2);


    var td3 = document.createElement("td");
    td3.setAttribute("style", "cursor: pointer; color: rgb(27, 105, 159);");
    td3.style.width = constants.VALIDDATE_COLUMN_WIDTH + "px";
    td3.setAttribute("class", "v-table-cell-content");
    tr.appendChild(td3);

    var tddiv3 = document.createElement("div");
    tddiv3.style.width = constants.VALIDDATE_COLUMN_WIDTH + "px";
    tddiv3.setAttribute("class", "v-table-cell-wrapper");
    tddiv3.textContent = formatDate(validToDate);
    td3.appendChild(tddiv3);


    tr.addEventListener("mouseup", function () {
        closeChooser();
        runSigning(hash);
    }, false);

}


/**
 * Показ окна выбора сертификата для подписания из списка личных сертификатов
 * пользователя.
 */
function showCertChooser2() {

    var certChooser = document.createElement("div");
    certChooser.setAttribute("id", constants.CERT_CHOOSER);
    certChooser.setAttribute("style", "overflow: visible ; top: 10%;  left: 10%;  height: 80%;  width: 80%; padding-top: 1px; padding-bottom: 9px;position: absolute;z-index: 20000;");
    certChooser.setAttribute("class", "v-panel v-widget v-has-width v-has-height");

    // Panel caption
    var pcaptionwrapper = document.createElement("div");
    pcaptionwrapper.setAttribute("class", "v-panel-captionwrap");
    var pcaption = document.createElement("div");
    pcaption.setAttribute("class", "v-panel-caption");
    pcaptionwrapper.appendChild(pcaption);
    var c = document.createElement("span");
    c.textContent = "Выбор сертификата";
    pcaption.appendChild(c);
    certChooser.appendChild(pcaptionwrapper);

    // Panel content
    var pcont = document.createElement("div");
    pcont.setAttribute("class", "v-panel-content v-scrollable");
    pcont.setAttribute("style", "overflow: auto;position: relative;height: 100%; width: 100%;");
    pcont.setAttribute("tabindex", "-1");

    var pcontvl = document.createElement("div");
    pcontvl.setAttribute("class", "v-verticallayout v-layout v-vertical v-widget v-has-width v-has-height v-margin-top v-margin-right v-margin-bottom v-margin-left");
    pcontvl.setAttribute("style", "height: 100%; width: 100%");

    var pcontvle = document.createElement("div");
    pcontvle.setAttribute("class", "v-expand");
    pcontvle.setAttribute("style", "padding-top: 0px;");

    var pcontvleslot = document.createElement("div");
    pcontvleslot.setAttribute("class", "v-slot v-align-center v-align-middle");
    pcontvleslot.setAttribute("style", "height: 90%; margin-top: 0px;");

    var listLayout = document.createElement("div");
    listLayout.setAttribute("class", "v-verticallayout v-layout v-vertical v-widget v-has-width v-has-height v-margin-top v-margin-right v-margin-bottom v-margin-left");
    listLayout.setAttribute("style", "height: 100%; width: 100%");

    var listExpand = document.createElement("div");
    listExpand.setAttribute("class", "v-expand");
    listExpand.setAttribute("style", "padding-top: 0px; position:relative;");

    var optionGroup = document.createElement("div");
    optionGroup.setAttribute("style", "overflow:auto; margin-top:20px;");

    var table = document.createElement("table");
    table.setAttribute("id", constants.CERTIFICATES_OPTION_GROUP);
    table.setAttribute("class", "certchooser");
//    table.setAttribute("cellspacing", "0");
//    table.setAttribute("cellpadding", "10px");
//    table.setAttribute("style", "width:100%; border-color: #c2c3c4;border-collapse: collapse; border-spacing: 0;");

    var tr = document.createElement("tr");
    //tr.setAttribute("style", "text-transform: uppercase;font-size: 10px;font-weight: bold;color: #222;text-shadow: #f3f5f8 0 1px 0;line-height: normal;background: #D9DADD;");

    var td_s = document.createElement("th");
    //td_s.setAttribute("class", "v-caption");
    //td_s.setAttribute("style", "border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid; position:absolute; top:-20px; z-index:2; height:20px; width:35%; ");
    td_s.textContent = "Выдан кому";

    var td_i = document.createElement("th");
    //td_i.setAttribute("class", "v-caption");
    //td_i.setAttribute("style", "border-left: 1px solid;border-top: 1px solid;border-bottom: 1px solid; position:absolute; top:-20px; z-index:2; height:20px; width:35%; ");
    td_i.textContent = "Выдан кем";

    var td_f = document.createElement("th");
    //td_f.setAttribute("class", "v-caption");
    //td_f.setAttribute("style", "border-left: 1px solid;border-top: 1px solid;border-bottom: 1px solid; position:absolute; top:-20px; z-index:2; height:20px; width:35%; ");
    td_f.textContent = "Действителен с";

    var td_t = document.createElement("th");
    //td_t.setAttribute("class", "v-caption");
    //td_t.setAttribute("style", "border-left: 1px solid;border-right: 1px solid;border-top: 1px solid;border-bottom: 1px solid; position:absolute; top:-20px; z-index:2; height:20px; width:35%; ");
    td_t.textContent = "Действителен по";

    tr.appendChild(td_s);
    tr.appendChild(td_i);
    tr.appendChild(td_f);
    tr.appendChild(td_t);
    table.appendChild(tr);
    optionGroup.appendChild(table);

    listExpand.appendChild(optionGroup);
    listLayout.appendChild(listExpand);
    pcontvleslot.appendChild(listLayout);
    pcontvle.appendChild(pcontvleslot);

    // Button in content
    var bslot = document.createElement("div");
    bslot.setAttribute("class", "v-slot v-align-center v-align-middle");
    bslot.setAttribute("style", "height: 10%; margin-top: 0px;");

    var bwidget = document.createElement("div");
    bwidget.setAttribute("class", "v-button v-widget");
    bwidget.setAttribute("role", "button");
    bwidget.setAttribute("tabindex", "0");

    var bwrapper = document.createElement("span");
    bwrapper.setAttribute("class", "v-button-wrap");
    bwrapper.addEventListener("mouseup", function () {
        closeChooser();
    }, false);

    var bcaption = document.createElement("span");
    bcaption.setAttribute("class", "v-button-caption");
    bcaption.textContent = "Отмена";

    bwrapper.appendChild(bcaption);
    bwidget.appendChild(bwrapper);
    bslot.appendChild(bwidget);
    pcontvle.appendChild(bslot);
    pcontvl.appendChild(pcontvle);
    pcont.appendChild(pcontvl);

    certChooser.appendChild(pcont);

    // Panel decoration
    var pdecoration = document.createElement("div");
    pdecoration.setAttribute("class", "v-panel-deco");
    pdecoration.setAttribute("style", "margin-bottom: -9px;");
    certChooser.appendChild(pdecoration);
    var d = document.getElementById('signForm');
    d.appendChild(certChooser);

    fillCertificatesList();
}

/**
 * Форматирование даты по ГОСТ Р 6.30-2003
 *
 * @param date
 * @returns {String}
 */
function formatDate(date) {
    var nd = date;
    if (isIE()) {
        nd = new Date(date);
    }
    var d = nd.getDate();
    var m = nd.getMonth() + 1;
    // 0-based
    var y = nd.getFullYear();
    var hh = nd.getHours();
    var mm = nd.getMinutes();
    var ss = nd.getSeconds();
    return (d < 10 ? "0" + d : d) + "." + (m < 10 ? "0" + m : m) + "." + y + " " + (hh < 10 ? "0" + hh : hh) + ":" + (mm < 10 ? "0" + mm : mm) + ":" + (ss < 10 ? "0" + ss : ss);

}

var switcher = false;
/**
 * Добавление сертификата в список доступных для подписывания
 *
 * @param hash -
 *                хэш сертификата, ключ для поиска сертификата в хранилище
 * @param subject -
 *                CN на кого выдан сертификат
 * @param issuer -
 *                CN кем выдан сертификат
 * @param validFromDate -
 *                сертифика валиден с
 * @param validToDate -
 *                сертифика валиден по
 */
function addCertificateOption2(hash, subject, issuer, validFromDate, validToDate) {
    switcher = !switcher;
    optionGroup = document.getElementById(constants.CERTIFICATES_OPTION_GROUP);
    if (optionGroup != null) {
        log("Adding to list certificate (SHA1 hash: " + hash + " Subject: " + subject + " Issuer: " + issuer + ")");

        tr = document.createElement("tr");
//	tr.setAttribute("style", "cursor: pointer;" + (switcher ? "background: #eff0f1;" : ""));

        td_s = document.createElement("td");
//	td_s.setAttribute("class", "v-caption");
//	td_s.setAttribute("style", "cursor: pointer; color: rgb(27, 105, 159); border-left: 1px solid;border-bottom: 1px solid;");
        td_s.textContent = subject;

        td_i = document.createElement("td");
//	td_i.setAttribute("class", "v-caption");
//	td_i.setAttribute("style", "cursor: pointer; color: rgb(27, 105, 159); border-left: 1px solid;border-bottom: 1px solid;");
        td_i.textContent = issuer;

        td_f = document.createElement("td");
//	td_f.setAttribute("class", "v-caption");
//	td_f.setAttribute("style", "cursor: pointer; color: rgb(27, 105, 159); border-left: 1px solid;border-bottom: 1px solid;");
        td_f.textContent = formatDate(validFromDate);

        td_t = document.createElement("td");
//	td_t.setAttribute("class", "v-caption");
//	td_t.setAttribute("style", "cursor: pointer; color: rgb(27, 105, 159); border-left: 1px solid;border-right: 1px solid;border-bottom: 1px solid;");
        td_t.textContent = formatDate(validToDate);

        tr.addEventListener("mouseup", function () {
            closeChooser();
            runSigning(hash);
        }, false);

        tr.appendChild(td_s);
        tr.appendChild(td_i);
        tr.appendChild(td_f);
        tr.appendChild(td_t);
        optionGroup.appendChild(tr);

    } else {
        log("Cannot find option group!");
    }
}

function log(message) {
    //console.info(message);
    ru.alfabank.orrpp.jscallback.log(message);

}

function logerror(message) {
    //console.error(message);
    ru.alfabank.orrpp.jscallback.showError(message);
}

function preparePluginObject(ieObj) {
    var success = false, wrapper;
    var cadesobject = document.getElementById(constants.CADES_OBJECT_ID);
    if (cadesobject || ieObj) {
        success = true;
    } else {
        switch (navigator.appName) {
            case 'Microsoft Internet Explorer':
                try {
                    var obj = new ActiveXObject("CAdESCOM.CPSigner");
                    success = true;
                } catch (err) {

                }
                break;
            default:
                var userAgent = navigator.userAgent;
                if (userAgent.match(/Trident\/./i)) { // IE10, 11
                    try {
                        obj = new ActiveXObject("CAdESCOM.CPSigner");
                        success = true;
                    } catch (err) {
                    }
                }
                if (!success) {
                    if (userAgent.match(/ipod/i) || userAgent.match(/ipad/i) || userAgent.match(/iphone/i)) {
                        wrapper = document.createElement('div');
                        wrapper.innerHTML = "<object id=\"" + constants.CADES_OBJECT_ID + "\" type=\"application/x-cades\" class=\"" + HTML_ELEMENT_CLASS + "\"></object>";
                        document.body.appendChild(wrapper);
                        success = true;
                        break;
                    }
                    cadesobject = document.getElementById('FFembeded');
                    var mimetype = navigator.mimeTypes["application/x-cades"];
                    if (mimetype) {
                        var plugin = mimetype.enabledPlugin;
                        if (plugin) {
                            wrapper = document.createElement('div');
                            wrapper.innerHTML = "<object id=\"" + constants.CADES_OBJECT_ID + "\" type=\"application/x-cades\" class=\"" + HTML_ELEMENT_CLASS + "\"></object>";
                            document.body.appendChild(wrapper);
                            success = true;
                        }
                    }
                }
        }
    }
    return success;
};


// *****************************************************************************
// ******************** ONLY FOR TEST PURPOSES ************************
// *****************************************************************************
var dataToBeSigned;// ONLY FOR TEST

function initTestSign() {
    init();
    prepareData({
        "requestText": "Test text! +HASHES",
        "requestId": 56555,
        "requestTextHash": "01F7FE4177AF2AE946732A5FA12DE326658E57FE7F7D701BE5F04D8BB317C1EA",
        "attachmentsHashes": [{
            "id": 111,
            "hash": "A85A7B8963E8DA190D5D1F073B20761E34FD553613FCDA8B7750A556F946DA26"
        }, {
            "id": 222,
            "hash": "0392092A86A765E685D71E020A5AAA5E5CB619005A9A9378DD73EFD4667D0A5B"
        }, {
            "id": 333,
            "hash": "9CBF38EDE997DC446F0C688FC271FE6D98D44E6DEFB17BD057AF9BB3A2CE11B4"
        }, {
            "id": 444,
            "hash": "944E29E56EB746ADAF1B9EF8CE24670164CBBCD0F6E90FECCE13D2F70AF21ADB"
        }, {
            "id": 555,
            "hash": "4184311C0A427E9CEB72CA29D3CD69D4CDCAC6B2D3266F8D020DBF15AE697941"
        }]
    });
    dataToBeSigned = requestText;
    var tbsText = document.getElementById("tbsText");
    tbsText.textContent = dataToBeSigned;

    log("Data TBS: " + dataToBeSigned);
    log("Data TBS hash:" + requestTextHash);

    log("Click button 'sign' to sign document...");

    console.dir(console);
}
