package ru.alfabank.urpip.grampus.debug;

public class WrapperForImpossibleException extends RuntimeException {
    
    private static final long serialVersionUID = 1L;
    
    public WrapperForImpossibleException(Throwable cause) {
        super("Impossible exception", cause);
    }
    
}
