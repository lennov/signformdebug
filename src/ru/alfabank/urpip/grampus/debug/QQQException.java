package ru.alfabank.urpip.grampus.debug;


//TODO #256 переименовать класс QQQException
public class QQQException extends RuntimeException {
    
    private static final long serialVersionUID = 1L;
    private String description;
    
    public QQQException(String message, String description, Throwable cause) {
        super(message, cause);
        this.description = description;
    }
    
    public QQQException(String message, Throwable cause) {
        this(message, null, cause);
    }
    
    public QQQException(String message, String description) {
        super(message);
        this.description = description;
    }
    
    public QQQException(String message) {
        this(message, (String) null);
    }
    
    public String getDescription() {
        return description;
    }
    
}
