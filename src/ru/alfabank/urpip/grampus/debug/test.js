 var CADESCOM_CADES_BES = 1;
        var CAPICOM_CURRENT_USER_STORE = 2;
        var CAPICOM_MY_STORE = "My";
        var CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED = 2;
        var CAPICOM_CERTIFICATE_FIND_SUBJECT_NAME = 1;

        function GetErrorMessage(e) {
            var err = e.message;
            if (!err) {
                err = e;
            } else if (e.number) {
                err += " (" + e.number + ")";
            }
            return err;
        }
        
        function SignCreate(certSubjectName, dataToSign) {
            return new Promise(function(resolve, reject){
                cadesplugin.async_spawn(function *(args) {
                    try {    
                        var oStore = yield cadesplugin.CreateObjectAsync("CAPICOM.Store");
                        yield oStore.Open(CAPICOM_CURRENT_USER_STORE, CAPICOM_MY_STORE,
                            CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

                        var CertificatesObj = yield oStore.Certificates;
                        var oCertificates = yield CertificatesObj.Find(
                            CAPICOM_CERTIFICATE_FIND_SUBJECT_NAME, certSubjectName);
                        
                        var Count = yield oCertificates.Count;
                        if (Count == 0) {
                            throw("Certificate not found: " + args[0]);
                        }
                        var oCertificate = yield oCertificates.Item(1);
                        var oSigner = yield cadesplugin.CreateObjectAsync("CAdESCOM.CPSigner");
                        yield oSigner.propset_Certificate(oCertificate);

                        var oSignedData = yield cadesplugin.CreateObjectAsync("CAdESCOM.CadesSignedData");
                        yield oSignedData.propset_Content(dataToSign);

                        var sSignedMessage = yield oSignedData.SignCades(oSigner, CADESCOM_CADES_BES);
        
                        yield oStore.Close();

                        args[2](sSignedMessage);
                    }
                    catch (e)
                    {
                        args[3]("Failed to create signature. Error: " + GetErrorMessage(e));
                    }
                }, certSubjectName, dataToSign, resolve, reject);
            });
        }

        function run() {
            var oCertName = document.getElementById("CertName");
            var sCertName = oCertName.value; 
            if ("" == sCertName) {
                alert("Введите имя сертификата (CN).");
                return;
            }
            var thenable = SignCreate(sCertName, "Message");

            thenable.then(
                function (result){
                    document.getElementById("signature").innerHTML = result;
                },
                function (result){
                    document.getElementById("signature").innerHTML = result;
                });
        }
        
        var async_code_included = 0;
        var async_Promise;
        var async_resolve;
        function include_async_code()
        {
            if(async_code_included)
            {
                return async_Promise;
            }
            var fileref = document.createElement('script');
            fileref.setAttribute("type", "text/javascript");
            fileref.setAttribute("src", "async_code.js");
            document.getElementsByTagName("head")[0].appendChild(fileref);
            async_Promise = new Promise(function(resolve, reject){
                async_resolve = resolve;
            });
            async_code_included = 1;
            return async_Promise;
        }

        
        function Common_CheckForPlugIn() {
            var canAsync = !!cadesplugin.CreateObjectAsync;
            if(canAsync)
            {
                include_async_code().then(function(){
                    return CheckForPlugIn_Async();
                });
            }else
            {
                return CheckForPlugIn_NPAPI();
            }
        }

        
        function CheckForPlugIn_Async() {
            function VersionCompare_Async(StringVersion, ObjectVersion)
            {
                if(typeof(ObjectVersion) == "string")
                    return -1;
                var arr = StringVersion.split('.');
                var isActualVersion = true;

                cadesplugin.async_spawn(function *() {
                    if((yield ObjectVersion.MajorVersion) == parseInt(arr[0]))
                    {
                        if((yield ObjectVersion.MinorVersion) == parseInt(arr[1]))
                        {
                            if((yield ObjectVersion.BuildVersion) == parseInt(arr[2]))
                            {
                                isActualVersion = true;
                            }
                            else if((yield ObjectVersion.BuildVersion) < parseInt(arr[2]))
                            {
                                isActualVersion = false;
                            }
                        }else if((yield ObjectVersion.MinorVersion) < parseInt(arr[1]))
                        {
                            isActualVersion = false;
                        }
                    }else if((yield ObjectVersion.MajorVersion) < parseInt(arr[0]))
                    {
                        isActualVersion = false;
                    }

                    if(!isActualVersion)
                    {
                        document.getElementById('PlugInEnabledTxt').innerHTML = "Плагин загружен, но есть более свежая версия.";
                    }
                    document.getElementById('PlugInVersionTxt').innerHTML = "Версия плагина: " + (yield CurrentPluginVersion.toString());
                    var oAbout = yield cadesplugin.CreateObjectAsync("CAdESCOM.About");
                    var ver = yield oAbout.CSPVersion("", 75);
                    var ret = (yield ver.MajorVersion) + "." + (yield ver.MinorVersion) + "." + (yield ver.BuildVersion);
                    document.getElementById('CSPVersionTxt').innerHTML = "Версия криптопровайдера: " + ret;
                    
                    try
                    {    
                        var sCSPName = yield oAbout.CSPName(75);
                        document.getElementById('CSPNameTxt').innerHTML = "Криптопровайдер: " + sCSPName;
                    }
                    catch(err){}
                    return;
                });
            }

            function GetLatestVersion_Async(CurrentPluginVersion) {
                var xmlhttp = getXmlHttp();
                xmlhttp.open("GET", "/sites/default/files/products/cades/latest_2_0.txt", true);
                xmlhttp.onreadystatechange = function() {
                var PluginBaseVersion;
                    if (xmlhttp.readyState == 4) {
                        if(xmlhttp.status == 200) {
                            PluginBaseVersion = xmlhttp.responseText;
                            VersionCompare_Async(PluginBaseVersion, CurrentPluginVersion) 
                        }
                    }
                }
                xmlhttp.send(null);
            }

            document.getElementById('PlugInEnabledTxt').innerHTML = "Плагин загружен.";
            var CurrentPluginVersion;
            cadesplugin.async_spawn(function *() {
                var oAbout = yield cadesplugin.CreateObjectAsync("CAdESCOM.About");
                CurrentPluginVersion = yield oAbout.PluginVersion;
                GetLatestVersion_Async(CurrentPluginVersion);
                if(location.pathname.indexOf("symalgo_sample.html")>=0){
                    FillCertList_Async('CertListBox1');
                    FillCertList_Async('CertListBox2');
                }else {
                    FillCertList_Async('CertListBox');
                }
                // var txtDataToSign = "Hello World";
                // document.getElementById("DataToSignTxtBox").innerHTML = txtDataToSign;
                // document.getElementById("SignatureTxtBox").innerHTML = "";
            }); //cadesplugin.async_spawn
        }
