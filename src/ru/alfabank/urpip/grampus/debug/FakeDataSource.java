package ru.alfabank.urpip.grampus.debug;

import com.vaadin.data.util.IndexedContainer;

public class FakeDataSource extends IndexedContainer {
    
    public FakeDataSource() {
        addContainerProperty("subject", String.class, null);
        addContainerProperty("issuer", String.class, null);
        addContainerProperty("validFrom", String.class, null);
        addContainerProperty("validTo", String.class, null);
        
        Object itemId;
        for (int i = 0; i <= 20; i++) {
            
            itemId = addItem();
            getContainerProperty(itemId, "subject").setValue("subject " + i);
            getContainerProperty(itemId, "issuer").setValue("subject " + i);
            getContainerProperty(itemId, "validFrom").setValue("validFrom " + i);
            getContainerProperty(itemId, "validTo").setValue("validTo " + i);
            
        }
    }
    
}
