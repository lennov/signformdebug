package ru.alfabank.urpip.grampus.debug;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import ru.alfabank.orrpp.common.util.Exceptions;
import ru.alfabank.urpip.debug.common.ClassifiedErrorException;

import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;

public abstract class AlfaUI extends UI {
    
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = Logger.getLogger(AlfaUI.class.getName());
    
    public AlfaUI() {
        setErrorHandler(new DefaultErrorHandler() {
            
            private static final long serialVersionUID = 1L;
            
            @Override
            public void error(com.vaadin.server.ErrorEvent event) {
                Throwable originalThrowable = event.getThrowable();
                String errorMessage;
                String errorDescription = null;
                Throwable meaningfulCause = Exceptions.getCause(originalThrowable, ClassifiedErrorException.class);
                if (meaningfulCause != null) {
                    errorMessage = meaningfulCause.getMessage();
                    errorDescription = "";
                } else if ((meaningfulCause = Exceptions.getCause(originalThrowable, UserNotificationException.class)) != null) {
                    errorMessage = meaningfulCause.getMessage();
                    String descr = ((UserNotificationException) meaningfulCause).getDescription();
                    if (descr != null) {
                        errorDescription = descr;
                    } else {
                        errorDescription = "";
                    }
                } else if ((meaningfulCause = Exceptions.getCause(originalThrowable, IOException.class)) != null) {
                    errorMessage = "Ошибка доступа к внешнему источнику";
                    close();
                } else {
                    meaningfulCause = originalThrowable;
                    errorMessage = "Внутренняя ошибка приложения";
                    errorDescription = "Обратитесь в службу поддержки";
                    close();
                }
                if (errorDescription == null) {
                    StringBuilder sb = new StringBuilder();
                    Throwable cause = meaningfulCause;
                    while (cause != null) {
                        sb.append(cause.getClass().getName());
                        String msg = Exceptions.getMessage(cause);
                        sb.append(msg == null ? "" : ": " + msg);
                        sb.append("\n");
                        cause = Exceptions.getDirectCause(cause);
                    }
                    errorDescription = sb.toString();
                }
                LOGGER.log(Level.SEVERE, errorMessage, Exceptions.createPrintableStub(originalThrowable));
                Notification.show(errorMessage + "\n", errorDescription, Type.ERROR_MESSAGE);
            }
        });
    }
    
    @Override
    abstract protected void init(VaadinRequest request);
}
