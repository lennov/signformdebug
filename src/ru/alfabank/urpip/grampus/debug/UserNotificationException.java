package ru.alfabank.urpip.grampus.debug;


//TODO DONE #256 переименовать класс QQQException
public class UserNotificationException extends RuntimeException {
    
    private static final long serialVersionUID = 1L;
    private String description;
    
    public UserNotificationException(String message, String description, Throwable cause) {
        super(message, cause);
        this.description = description;
    }
    
    public UserNotificationException(String message, Throwable cause) {
        this(message, null, cause);
    }
    
    public UserNotificationException(String message, String description) {
        super(message);
        this.description = description;
    }
    
    public UserNotificationException(String message) {
        this(message, (String) null);
    }
    
    public String getDescription() {
        return description;
    }
    
}
