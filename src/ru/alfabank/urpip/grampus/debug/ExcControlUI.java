package ru.alfabank.urpip.grampus.debug;

import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.vaadin.annotations.JavaScript;
import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

@Theme("exchangecontroluitheme")
@JavaScript({"es6-promise.min.js", "ie_eventlistner_polyfill.js", "cadesplugin_api.js", "Code.js", "async_code.js"})
@PreserveOnRefresh
public class ExcControlUI extends AlfaUI {
    
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = Logger.getLogger(ExcControlUI.class.getName());
    
    //private SignForm2 signForm2;
    
    @Override
    public void init(VaadinRequest request) {
        try {
            Context c = new InitialContext();
            
            Panel panel = new Panel("Test sign panel caption");
            
            VerticalLayout content = new VerticalLayout();
            content.addComponent(panel);
            Button signByOldPlugin = new Button("Sign by old plugin", new Button.ClickListener() {
                
                @Override
                public void buttonClick(ClickEvent event) {
                    getUI().getCurrent().addWindow(new SignForm());
                }
            });
            
            //signForm2 = new SignForm2();
            Button signByNewPlugin = new Button("Sign by new plugin", new Button.ClickListener() {
                
                @Override
                public void buttonClick(ClickEvent event) {
                    SignForm2 signForm2 = new SignForm2();
                    getUI().getCurrent().addWindow(signForm2);
                }
            });
            Button generateNewFormButton = new Button("Generate new form", new Button.ClickListener() {
                
                @Override
                public void buttonClick(ClickEvent event) {
                    getUI().getCurrent().addWindow(new NewForm());
                }
            });
            
            VerticalLayout btnLayout = new VerticalLayout();
            btnLayout.addComponent(signByOldPlugin);
            btnLayout.addComponent(signByNewPlugin);
            btnLayout.addComponent(generateNewFormButton);
            
            panel.setContent(btnLayout);
            
            setContent(content);
            
        } catch (NamingException e) {//new RKServiceFacadeStub();//
            LOGGER.severe("Error on looking up EJB beans: " + e.getMessage());
            throw new RuntimeException("Cannot lookup beans!", e);
        }
        
    }
}
