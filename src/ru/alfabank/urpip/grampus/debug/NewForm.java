package ru.alfabank.urpip.grampus.debug;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class NewForm extends Window {
    
    public NewForm() {
        setWidth("800px");
        setHeight("400px");
        setModal(true);
        setResizable(false);
        center();
        Panel panel = new Panel("Выбор ключа для подписания");
        panel.setSizeFull();
        setContent(panel);
        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setMargin(true);
        mainLayout.setSpacing(true);
        mainLayout.setSizeFull();
        
        panel.setContent(mainLayout);
        
        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.setSizeFull();
        VerticalLayout btnLayout = new VerticalLayout();
        btnLayout.setSizeFull();
        mainLayout.addComponent(tableLayout);
        mainLayout.addComponent(btnLayout);
        
        mainLayout.setExpandRatio(tableLayout, 0.9f);
        mainLayout.setExpandRatio(btnLayout, 0.1f);
        
        Table table = new Table();
        table.setSortEnabled(false);
        table.setSizeFull();
        table.setContainerDataSource(new FakeDataSource());
        Button btnCancel = new Button("Отмена");
        
        tableLayout.addComponent(table);
        tableLayout.setExpandRatio(table, 1.0f);
        btnLayout.addComponent(btnCancel);
        btnLayout.setComponentAlignment(btnCancel, Alignment.MIDDLE_CENTER);
    }
    
}
