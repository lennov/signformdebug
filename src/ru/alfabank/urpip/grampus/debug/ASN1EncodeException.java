package ru.alfabank.urpip.grampus.debug;

import codec.asn1.ASN1Type;

public class ASN1EncodeException extends Exception {
    
    private static final long serialVersionUID = 1L;
    private Class<? extends ASN1Type> asn1AwareObjectClass;
    
    public ASN1EncodeException(String string, Class<? extends ASN1Type> asn1AwareObjectClass) {
        super(string);
        this.asn1AwareObjectClass = asn1AwareObjectClass;
    }
    
    public ASN1EncodeException(String string, Class<? extends ASN1Type> asn1AwareObjectClass, Exception cause) {
        super(string, cause);
        this.asn1AwareObjectClass = asn1AwareObjectClass;
    }
    
    public Class<? extends ASN1Type> getAsn1AwareObjectClass() {
        return asn1AwareObjectClass;
    }
    
}
