package ru.alfabank.urpip.debug.common;

public class StringUtils {
    
    public static String trimStringToLength(String source, int maxLength) {
        assert maxLength > 0;
        assert source != null;
        String result = source;
        
        if (source.length() > maxLength) {
            result = source.substring(0, maxLength / 2 - 2);
            result += "...";
            result += source.substring(source.length() - maxLength / 2 + 1, source.length());
        }
        
        return result;
    }
    
    public static void main(String[] args) {
        int toSize = 50;
        String res = trimStringToLength("Ну очень длинное название файла просто какое то невероятно длинное, которое точно не поместиться ни в одно поле и ни в одно окно, поэтому интересно отладиться именно на нем", toSize);
        System.out.println("Length: " + res.length());
        System.out.println("Result: " + res);
    }
}
