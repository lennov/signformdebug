package ru.alfabank.urpip.debug.common;

/**
 * 
 * Типы документов для подписания. Нужны для определения источника данных в форме подписания.
 * 
 * TODO LEN исправить названия типов документов для подписания на нормальные
 */
public enum TBSDocumentType {
    CURRENCY_OPERATION_CERTIFICATE,
    CONFIRM_CERTIFICATE,
    SIMPLE_REQUEST,
    PASSPORT,
    PSKD,
    //Паспорт сделки по контракту
    
    //
    ;
}
