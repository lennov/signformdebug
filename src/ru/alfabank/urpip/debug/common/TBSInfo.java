package ru.alfabank.urpip.debug.common;

import java.util.List;

public class TBSInfo {
    
    private int idState;
    private String tbsText;
    private List<CurrencyDocumentAttachment> attachements;
    private TBSDocumentType type;
    
    public String getTbsText() {
        return tbsText;
    }
    
    public byte[] getTbsBytes() {
        return tbsText.getBytes();
    }
    
    public void setTbsText(String tbsText) {
        this.tbsText = tbsText;
    }
    
    public List<CurrencyDocumentAttachment> getAttachements() {
        return attachements;
    }
    
    public void setAttachements(List<CurrencyDocumentAttachment> attachments) {
        this.attachements = attachments;
    }
    
    public int getIdState() {
        return idState;
    }
    
    public void setIdState(int id) {
        this.idState = id;
    }
    
    public TBSDocumentType getType() {
        return type;
    }
    
    public void setType(TBSDocumentType type) {
        this.type = type;
    }
    
}
