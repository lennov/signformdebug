package ru.alfabank.urpip.debug.common;

import java.util.Arrays;

public class CurrencyDocumentAttachment {
    
    private int id;
    private String filename;
    private String authorName;
    private byte[] signatures;
    private long size;
    private byte[] sha1Digest;
    private byte[] gostDigest;
    private byte[] content;
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getFilename() {
        return filename;
    }
    
    public void setFilename(String filename) {
        this.filename = filename;
    }
    
    public String getAuthorName() {
        return authorName;
    }
    
    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
    
    public byte[] getSignatures() {
        return signatures;
    }
    
    public void setSignatures(byte[] signatures) {
        this.signatures = signatures;
    }
    
    public void setSize(long size) {
        this.size = size;
    }
    
    public long getSize() {
        return this.size;
    }
    
    public void setSHA1Digest(byte[] digest) {
        this.sha1Digest = digest;
    }
    
    public byte[] getSHA1Digest() {
        return sha1Digest;
    }
    
    public void setGOSTDigest(byte[] digest) {
        this.gostDigest = digest;
    }
    
    public byte[] getGOSTDigest() {
        return gostDigest;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CurrencyDocumentAttachment [id=");
        builder.append(id);
        builder.append(", ");
        if (filename != null) {
            builder.append("filename=");
            builder.append(filename);
            builder.append(", ");
        }
        builder.append("size=");
        builder.append(size);
        builder.append(", ");
        if (authorName != null) {
            builder.append("authorName=");
            builder.append(authorName);
            builder.append(", ");
        }
        if (signatures != null) {
            builder.append("signatures=");
            builder.append(Arrays.toString(signatures));
            builder.append(", ");
        }
        if (sha1Digest != null) {
            builder.append("sha1Digest=");
            builder.append(Arrays.toString(sha1Digest));
        }
        if (gostDigest != null) {
            builder.append("gostDigest=");
            builder.append(Arrays.toString(gostDigest));
        }
        builder.append("]");
        return builder.toString();
    }
    
    public byte[] getContent() {
        return content;
    }
    
    public void setContent(byte[] content) {
        this.content = content;
    }
    
}
