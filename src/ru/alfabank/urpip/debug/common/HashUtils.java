package ru.alfabank.urpip.debug.common;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import ru.CryptoPro.JCP.params.OID;

public class HashUtils {
    
    public static byte[] computeGOSTHash(byte[] bytes) throws NoSuchAlgorithmException, NoSuchProviderException {
    	Security.addProvider(new BouncyCastleProvider());
        MessageDigest gmd = MessageDigest.getInstance("1.2.643.2.2.9", "BC");
        gmd.reset();
        gmd.update(bytes);
        byte[] gostdigest = gmd.digest();
        return gostdigest;
    }
    
    public static byte[] computeHash(byte[] bytes, String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        MessageDigest md = MessageDigest.getInstance(algorithm);
        md.reset();
        md.update(bytes);
        byte[] digest = md.digest();
        return digest;
    }
    
}
