package ru.alfabank.urpip.debug.common;


public class AttachmentInfo {
    
    public int id;
    public String hash;
    
    public AttachmentInfo(int id, String hash) {
        this.id = id;
        this.hash = hash;
    }
    
}
