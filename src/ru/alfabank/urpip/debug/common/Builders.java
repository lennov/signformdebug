package ru.alfabank.urpip.debug.common;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Container;
import com.vaadin.data.Validator;
import com.vaadin.server.Resource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class Builders {
    
    public static abstract class AbstractComponentBuilder<T> {
        
        protected AbstractComponent result;
        protected T builderClass;
        
        public T addStyleName(String styleName) {
            result.addStyleName(styleName);
            return builderClass;
        }
        
        public T width(String width) {
            result.setWidth(width);
            return builderClass;
        }
        
        public T height(String height) {
            result.setHeight(height);
            return builderClass;
        }
        
        public T enabled(boolean enabled) {
            result.setEnabled(enabled);
            return builderClass;
        }
        
        public T immediate(boolean immediate) {
            result.setImmediate(immediate);
            return builderClass;
        }
        
        public T caption(String caption) {
            result.setCaption(caption);
            return builderClass;
        }
        
        public T description(String description) {
            result.setDescription(description);
            return builderClass;
        }
        
        public T visible(Boolean visible) {
            result.setVisible(visible);
            return builderClass;
        }
    }
    
    public static abstract class AbstractLayoutBuilder<T> extends AbstractComponentBuilder<T> {
        
        protected List<AbstractComponent> layoutComponents = new ArrayList<AbstractComponent>();
        
        public T margin(Boolean margin) {
            ((AbstractOrderedLayout) result).setMargin(margin);
            return builderClass;
        }
        
        public T sizeFull() {
            ((AbstractOrderedLayout) result).setSizeFull();
            return builderClass;
        }
        
        public T spacing(Boolean spacing) {
            ((AbstractOrderedLayout) result).setSpacing(spacing);
            return builderClass;
        }
        
        public T margin(MarginInfo marginInfo) {
            ((AbstractOrderedLayout) result).setMargin(marginInfo);
            return builderClass;
        }
        
        public T addComponent(AbstractComponent component) {
            ((AbstractOrderedLayout) result).addComponent(component);
            return builderClass;
        }
        
        public T componentAlignment(Component childComponent, Alignment alignment) {
            ((AbstractOrderedLayout) result).setComponentAlignment(childComponent, alignment);
            return builderClass;
        }
        
        public T expandRatio(Component childComponent, float ratio) {
            ((AbstractOrderedLayout) result).setExpandRatio(childComponent, ratio);
            return builderClass;
        }
        
    }
    
    public static abstract class AbstractFieldBuilder<T> extends AbstractComponentBuilder<T> {
        
        public T validationVisible(Boolean validationVisible) {
            ((AbstractField) result).setValidationVisible(validationVisible);
            return builderClass;
        }
        
        public T required(boolean required) {
            ((AbstractField) result).setRequired(required);
            return builderClass;
        }
        
        public T requiredError(String requiredError) {
            ((AbstractField) result).setRequiredError(requiredError);
            return builderClass;
        }
        
        public T validator(Validator validator) {
            ((AbstractField) result).addValidator(validator);
            return builderClass;
        }
        
        public T value(Object value) {
            ((AbstractField) result).setValue(value);
            return builderClass;
        }
    }
    
    public static class VerticalLayoutBuilder extends AbstractLayoutBuilder<VerticalLayoutBuilder> {
        
        public VerticalLayoutBuilder() {
            super();
            builderClass = this;
            result = new VerticalLayout();
        }
        
        public VerticalLayout build() {
            return (VerticalLayout) result;
        }
    }
    
    public static class ButtonBuilder extends AbstractComponentBuilder<ButtonBuilder> {
        
        public ButtonBuilder(String caption) {
            super();
            builderClass = this;
            result = new Button(caption);
        }
        
        public Button build() {
            return (Button) result;
        }
    }
    
    public static class HorizLayoutBuilder extends AbstractLayoutBuilder<HorizLayoutBuilder> {
        
        public HorizLayoutBuilder() {
            super();
            builderClass = this;
            result = new HorizontalLayout();
        }
        
        public HorizontalLayout build() {
            return (HorizontalLayout) result;
        }
    }
    
    public static class GridLayoutBuilder extends AbstractComponentBuilder<GridLayoutBuilder> {
        
        public GridLayoutBuilder(int columns, int rows) {
            super();
            builderClass = this;
            result = new GridLayout(columns, rows);
        }
        
        public GridLayoutBuilder margin(Boolean margin) {
            ((GridLayout) result).setMargin(margin);
            return builderClass;
        }
        
        public GridLayoutBuilder spacing(Boolean spacing) {
            ((GridLayout) result).setSpacing(spacing);
            return builderClass;
        }
        
        public GridLayoutBuilder margin(MarginInfo marginInfo) {
            ((GridLayout) result).setMargin(marginInfo);
            return builderClass;
        }
        
        public GridLayoutBuilder addComponent(AbstractComponent component) {
            ((GridLayout) result).addComponent(component);
            return builderClass;
        }
        
        public GridLayoutBuilder componentAlignment(Component childComponent, Alignment alignment) {
            ((GridLayout) result).setComponentAlignment(childComponent, alignment);
            return builderClass;
        }
        
        public GridLayout build() {
            return (GridLayout) result;
        }
    }
    
    public static class LabelBuilder extends AbstractComponentBuilder<LabelBuilder> {
        
        public LabelBuilder(String title) {
            builderClass = this;
            result = new Label(title);
        }
        
        public LabelBuilder(String title, ContentMode contentMode) {
            builderClass = this;
            result = new Label(title, contentMode);
        }
        
        public Label build() {
            return (Label) result;
        }
    }
    
    public static class CheckBoxBuilder extends AbstractFieldBuilder<CheckBoxBuilder> {
        
        public CheckBoxBuilder() {
            builderClass = this;
            result = new CheckBox();
        }
        
        public CheckBox build() {
            return (CheckBox) result;
        }
    }
    
    public static class TextFieldBuilder extends AbstractFieldBuilder<TextFieldBuilder> {
        
        public TextFieldBuilder() {
            super();
            builderClass = this;
            result = new TextField();
        }
        
        public TextFieldBuilder maxLength(int maxLength) {
            ((TextField) result).setMaxLength(maxLength);
            return this;
        }
        
        public TextFieldBuilder nullRepresentation(String nullRepresentation) {
            ((TextField) result).setNullRepresentation(nullRepresentation);
            return this;
        }
        
        public TextField build() {
            return (TextField) result;
        }
    }
    
    public static class TextAreaBuilder extends AbstractFieldBuilder<TextAreaBuilder> {
        
        public TextAreaBuilder() {
            super();
            builderClass = this;
            result = new TextArea();
        }
        
        public TextAreaBuilder maxLength(int maxLength) {
            ((TextArea) result).setMaxLength(maxLength);
            return this;
        }
        
        public TextAreaBuilder rows(int rows) {
            ((TextArea) result).setRows(rows);
            return this;
        }
        
        public TextAreaBuilder nullRepresentation(String nullRepresentation) {
            ((TextArea) result).setNullRepresentation(nullRepresentation);
            return this;
        }
        
        public TextArea build() {
            return (TextArea) result;
        }
    }
    
    public static class DateFieldBuilder extends AbstractFieldBuilder<DateFieldBuilder> {
        
        public DateFieldBuilder() {
            super();
            builderClass = this;
            result = new DateField();
        }
        
        public DateFieldBuilder dateFormat(String dateFormat) {
            ((DateField) result).setDateFormat(dateFormat);
            return this;
        }
        
        public DateFieldBuilder parseErrorMessage(String parseErrorMessage) {
            ((DateField) result).setParseErrorMessage(parseErrorMessage);
            return this;
        }
        
        public DateField build() {
            return (DateField) result;
        }
    }
    
    public static class DateFieldPopupBuilder extends AbstractFieldBuilder<DateFieldPopupBuilder> {
        
        public DateFieldPopupBuilder() {
            super();
            builderClass = this;
            result = new PopupDateField();
        }
        
        public DateFieldPopupBuilder dateFormat(String dateFormat) {
            ((DateField) result).setDateFormat(dateFormat);
            return this;
        }
        
        public DateFieldPopupBuilder parseErrorMessage(String parseErrorMessage) {
            ((DateField) result).setParseErrorMessage(parseErrorMessage);
            return this;
        }
        
        public PopupDateField build() {
            return (PopupDateField) result;
        }
    }
    
    public static class ComboBoxBuilder extends AbstractFieldBuilder<ComboBoxBuilder> {
        
        public ComboBoxBuilder() {
            this(null);
        }
        
        public ComboBoxBuilder(Container dataSource) {
            super();
            builderClass = this;
            if (dataSource == null) {
                result = new ComboBox();
            } else {
                result = new ComboBox(null, dataSource);
            }
        }
        
        public ComboBoxBuilder nullSelectionItemId(Object nullSelectionItemId) {
            ((ComboBox) result).setNullSelectionItemId(nullSelectionItemId);
            return this;
        }
        
        public ComboBoxBuilder container(Container dataSource) {
            ((ComboBox) result).setContainerDataSource(dataSource);
            return this;
        }
        
        public ComboBoxBuilder nullSelectionAllowed(boolean nullSelectionAllowed) {
            ((ComboBox) result).setNullSelectionAllowed(nullSelectionAllowed);
            return this;
        }
        
        public ComboBoxBuilder itemCaptionPropertyId(String itemCaptionPropertyId) {
            ((ComboBox) result).setItemCaptionPropertyId(itemCaptionPropertyId);
            return this;
        }
        
        public ComboBoxBuilder filteringMode(FilteringMode filteringMode) {
            ((ComboBox) result).setFilteringMode(filteringMode);
            return this;
        }
        
        public ComboBox build() {
            return (ComboBox) result;
        }
    }
    
    public static class EmbeddedBuilder extends AbstractComponentBuilder<EmbeddedBuilder> {
        
        public EmbeddedBuilder(Resource resource) {
            super();
            builderClass = this;
            result = new Embedded(null, resource);
        }
        
        public Embedded build() {
            return (Embedded) result;
        }
    }
    
}
