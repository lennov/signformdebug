package ru.crypto.example;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.spec.ECGenParameterSpec;

import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.GOST3410ParameterSpec;

import sun.security.pkcs10.PKCS10;
import sun.security.x509.X500Name;

/**
 * This class generates PKCS10 certificate signing request
 *
 * @author Pankaj@JournalDev.com
 * @version 1.0
 */
public class GenerateCSR {
	private static PublicKey publicKey = null;
	private static PrivateKey privateKey = null;
	private static KeyPairGenerator keyGen = null;
	private static GenerateCSR gcsr = null;

	private GenerateCSR() throws NoSuchProviderException, InvalidAlgorithmParameterException {
		try {
			keyGen = KeyPairGenerator.getInstance("GOST3410", "BC");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		GOST3410ParameterSpec gost3410P = new GOST3410ParameterSpec(
				CryptoProObjectIdentifiers.gostR3410_94_CryptoPro_B.getId());
		keyGen.initialize(gost3410P);

		KeyPair keypair = keyGen.generateKeyPair();
		publicKey = keypair.getPublic();
		privateKey = keypair.getPrivate();
	}

	public static GenerateCSR getInstance() throws NoSuchProviderException, InvalidAlgorithmParameterException {
		if (gcsr == null)
			gcsr = new GenerateCSR();
		return gcsr;
	}

	public String getCSR(String cn) throws Exception {
		byte[] csr = generatePKCS10(cn, "Java", "JournalDev", "Cupertino", "California", "USA");
		return new String(csr);
	}

	/**
	 *
	 * @param CN
	 *            Common Name, is X.509 speak for the name that distinguishes
	 *            the Certificate best, and ties it to your Organization
	 * @param OU
	 *            Organizational unit
	 * @param O
	 *            Organization NAME
	 * @param L
	 *            Location
	 * @param S
	 *            State
	 * @param C
	 *            Country
	 * @return
	 * @throws Exception
	 */
	private static byte[] generatePKCS10(String CN, String OU, String O, String L, String S, String C)
			throws Exception {
		// generate PKCS10 certificate request
		PKCS10 pkcs10 = new PKCS10(publicKey);
		Signature signature = Signature.getInstance("GOST3411WITHGOST3410", "BC");
		System.out.println(signature.getAlgorithm());
		signature.initSign(privateKey);
		// common, orgUnit, org, locality, state, country
		X500Name x500Name = new X500Name(CN, OU, O, L, S, C);
		pkcs10.encodeAndSign(x500Name, signature);
		ByteArrayOutputStream bs = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(bs);
		pkcs10.print(ps);
		byte[] c = bs.toByteArray();
		try {
			if (ps != null)
				ps.close();
			if (bs != null)
				bs.close();
		} catch (Throwable th) {
		}
		return c;
	}

	public PublicKey getPublicKey() {
		return publicKey;
	}

	public PrivateKey getPrivateKey() {
		return privateKey;
	}

	public static void main(String[] args) throws Exception {
		Security.addProvider(new BouncyCastleProvider());

		GenerateCSR gcsr = GenerateCSR.getInstance();

		System.out.println("Public Key:\n" + gcsr.getPublicKey().toString());

		System.out.println("Private Key:\n" + gcsr.getPrivateKey().toString());
		String csr = gcsr.getCSR("journaldev.com <http://www.journaldev.com>");
		System.out.println("CSR Request Generated!!");
		System.out.println(csr);
	}

}