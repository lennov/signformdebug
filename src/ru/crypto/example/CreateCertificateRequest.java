package ru.crypto.example;

import java.nio.file.Paths;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.Provider;
import java.security.Provider.Service;
import java.security.Security;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.ECGenParameterSpec;
import java.util.Set;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.google.gwt.thirdparty.guava.common.io.Files;

public class CreateCertificateRequest {

	public static void main(String[] args) {
		Security.addProvider(new BouncyCastleProvider());

		try {
			Provider provider = Security.getProvider("BC");

			Set<Service> services = provider.getServices();
			for (Service s : services) {
				if ("KeyFactory".equals(s.getType())) {
					System.out.println("Service " + s.toString());
				}
			}

			Service keyFactoryService = provider.getService("KeyFactory", "ECGOST3410");
			
			KeyPairGenerator kpg = KeyPairGenerator.getInstance("ECGOST3410", "BC");
			kpg.initialize(new ECGenParameterSpec("GostR3410-2001-CryptoPro-A"));

			KeyPair pair = kpg.generateKeyPair();
			
			X500Principal subjectName = new X500Principal("CN=Test V3 Certificate,SURNAME=Lennov,GIVENNAME=Roman,T=developer");
			PKCS10CertificationRequest kpGen = new PKCS10CertificationRequest("GOST3411withECGOST3410", subjectName,
					pair.getPublic(), null, pair.getPrivate());
			
			byte[] derEncoded = kpGen.getEncoded();
			
			System.out.println("CSR: " + new String(derEncoded));
			
			Files.write(derEncoded, Paths.get("w:\\tmp\\"+System.currentTimeMillis()+"_cert_request.csr").toFile());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
